sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 512MiB each
2GiB total file size
Block size 128KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     583.78
    fsyncs/s:                     23.55

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               72.97

General statistics:
    total time:                          10.1049s
    total number of events:              6134

Latency (ms):
         min:                                    0.06
         avg:                                    1.65
         max:                                  220.00
         95th percentile:                        0.17
         sum:                                10099.79

Threads fairness:
    events (avg/stddev):           6134.0000/0.00
    execution time (avg/stddev):   10.0998/0.00

