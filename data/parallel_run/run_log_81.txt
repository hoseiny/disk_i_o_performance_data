sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 2GiB each
8GiB total file size
Block size 512KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     193.00
    fsyncs/s:                     7.82

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               96.50

General statistics:
    total time:                          10.3587s
    total number of events:              2077

Latency (ms):
         min:                                    0.19
         avg:                                    4.99
         max:                                  548.29
         95th percentile:                        0.60
         sum:                                10355.76

Threads fairness:
    events (avg/stddev):           2077.0000/0.00
    execution time (avg/stddev):   10.3558/0.00

