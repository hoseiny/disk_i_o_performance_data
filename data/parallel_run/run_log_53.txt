sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 2GiB each
8GiB total file size
Block size 16KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     1724.49
    fsyncs/s:                     69.08

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               26.95

General statistics:
    total time:                          10.0304s
    total number of events:              17989

Latency (ms):
         min:                                    0.01
         avg:                                    0.56
         max:                                   92.21
         95th percentile:                        0.05
         sum:                                10019.51

Threads fairness:
    events (avg/stddev):           17989.0000/0.00
    execution time (avg/stddev):   10.0195/0.00

