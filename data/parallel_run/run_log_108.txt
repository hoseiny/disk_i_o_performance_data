sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
16 files, 256MiB each
4GiB total file size
Block size 128KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     414.02
    fsyncs/s:                     66.54

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               51.75

General statistics:
    total time:                          10.1405s
    total number of events:              4859

Latency (ms):
         min:                                    0.06
         avg:                                    2.09
         max:                                  243.93
         95th percentile:                        0.26
         sum:                                10131.90

Threads fairness:
    events (avg/stddev):           4859.0000/0.00
    execution time (avg/stddev):   10.1319/0.00

