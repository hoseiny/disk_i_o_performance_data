sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
1 files, 1GiB each
1GiB total file size
Block size 4KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     3004.86
    fsyncs/s:                     30.15

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               11.74

General statistics:
    total time:                          10.0135s
    total number of events:              30401

Latency (ms):
         min:                                    0.00
         avg:                                    0.33
         max:                                  258.61
         95th percentile:                        0.01
         sum:                                10000.43

Threads fairness:
    events (avg/stddev):           30401.0000/0.00
    execution time (avg/stddev):   10.0004/0.00

