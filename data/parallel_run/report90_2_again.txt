[172.18.23.226] Executing task 'check'
[172.18.23.226] sudo: sysbench --test=fileio --file-total-size=2G --file-test-mode=rndrw --max-time=90 --max-requests=0 run
[172.18.23.226] out: sudo password:
[172.18.23.226] out: WARNING: the --test option is deprecated. You can pass a script name or path on the command line without any options.
[172.18.23.226] out: WARNING: --max-time is deprecated, use --time instead
[172.18.23.226] out: sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)
[172.18.23.226] out: 
[172.18.23.226] out: Running the test with following options:
[172.18.23.226] out: Number of threads: 1
[172.18.23.226] out: Initializing random number generator from current time
[172.18.23.226] out: 
[172.18.23.226] out: 
[172.18.23.226] out: Extra file open flags: 0
[172.18.23.226] out: 128 files, 16MiB each
[172.18.23.226] out: 2GiB total file size
[172.18.23.226] out: Block size 16KiB
[172.18.23.226] out: Number of IO requests: 0
[172.18.23.226] out: Read/Write ratio for combined random IO test: 1.50
[172.18.23.226] out: Periodic FSYNC enabled, calling fsync() each 100 requests.
[172.18.23.226] out: Calling fsync() at the end of test, Enabled.
[172.18.23.226] out: Using synchronous I/O mode
[172.18.23.226] out: Doing random r/w test
[172.18.23.226] out: Initializing worker threads...
[172.18.23.226] out: 
[172.18.23.226] out: Threads started!
[172.18.23.226] out: 
[172.18.23.226] out: 
[172.18.23.226] out: File operations:
[172.18.23.226] out:     reads/s:                      77.33
[172.18.23.226] out:     writes/s:                     51.55
[172.18.23.226] out:     fsyncs/s:                     164.69
[172.18.23.226] out: 
[172.18.23.226] out: Throughput:
[172.18.23.226] out:     read, MiB/s:                  1.21
[172.18.23.226] out:     written, MiB/s:               0.81
[172.18.23.226] out: 
[172.18.23.226] out: General statistics:
[172.18.23.226] out:     total time:                          90.0044s
[172.18.23.226] out:     total number of events:              26423
[172.18.23.226] out: 
[172.18.23.226] out: Latency (ms):
[172.18.23.226] out:          min:                                  0.00
[172.18.23.226] out:          avg:                                  3.40
[172.18.23.226] out:          max:                                204.87
[172.18.23.226] out:          95th percentile:                     24.38
[172.18.23.226] out:          sum:                              89956.90
[172.18.23.226] out: 
[172.18.23.226] out: Threads fairness:
[172.18.23.226] out:     events (avg/stddev):           26423.0000/0.00
[172.18.23.226] out:     execution time (avg/stddev):   89.9569/0.00
[172.18.23.226] out: 
[172.18.23.226] out: 

[172.18.23.230] Executing task 'check'
[172.18.23.230] sudo: sysbench --test=fileio --file-total-size=2G --file-test-mode=rndrw --max-time=90 --max-requests=0 run
[172.18.23.230] out: sudo password:
[172.18.23.230] out: WARNING: the --test option is deprecated. You can pass a script name or path on the command line without any options.
[172.18.23.230] out: WARNING: --max-time is deprecated, use --time instead
[172.18.23.230] out: sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)
[172.18.23.230] out: 
[172.18.23.230] out: Running the test with following options:
[172.18.23.230] out: Number of threads: 1
[172.18.23.230] out: Initializing random number generator from current time
[172.18.23.230] out: 
[172.18.23.230] out: 
[172.18.23.230] out: Extra file open flags: 0
[172.18.23.230] out: 128 files, 16MiB each
[172.18.23.230] out: 2GiB total file size
[172.18.23.230] out: Block size 16KiB
[172.18.23.230] out: Number of IO requests: 0
[172.18.23.230] out: Read/Write ratio for combined random IO test: 1.50
[172.18.23.230] out: Periodic FSYNC enabled, calling fsync() each 100 requests.
[172.18.23.230] out: Calling fsync() at the end of test, Enabled.
[172.18.23.230] out: Using synchronous I/O mode
[172.18.23.230] out: Doing random r/w test
[172.18.23.230] out: Initializing worker threads...
[172.18.23.230] out: 
[172.18.23.230] out: Threads started!
[172.18.23.230] out: 
[172.18.23.230] out: 
[172.18.23.230] out: File operations:
[172.18.23.230] out:     reads/s:                      364.57
[172.18.23.230] out:     writes/s:                     243.05
[172.18.23.230] out:     fsyncs/s:                     776.38
[172.18.23.230] out: 
[172.18.23.230] out: Throughput:
[172.18.23.230] out:     read, MiB/s:                  5.70
[172.18.23.230] out:     written, MiB/s:               3.80
[172.18.23.230] out: 
[172.18.23.230] out: General statistics:
[172.18.23.230] out:     total time:                          90.0194s
[172.18.23.230] out:     total number of events:              124593
[172.18.23.230] out: 
[172.18.23.230] out: Latency (ms):
[172.18.23.230] out:          min:                                  0.00
[172.18.23.230] out:          avg:                                  0.72
[172.18.23.230] out:          max:                                227.98
[172.18.23.230] out:          95th percentile:                      0.75
[172.18.23.230] out:          sum:                              89869.92
[172.18.23.230] out: 
[172.18.23.230] out: Threads fairness:
[172.18.23.230] out:     events (avg/stddev):           124593.0000/0.00
[172.18.23.230] out:     execution time (avg/stddev):   89.8699/0.00
[172.18.23.230] out: 
[172.18.23.230] out: 


Done.
Disconnecting from 172.18.23.230... done.
Disconnecting from 172.18.23.226... done.
