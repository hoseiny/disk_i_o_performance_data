sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 16GiB each
64GiB total file size
Block size 512KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     159.56
    fsyncs/s:                     6.70

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               79.78

General statistics:
    total time:                          10.1430s
    total number of events:              1683

Latency (ms):
         min:                                    0.19
         avg:                                    5.94
         max:                                  637.52
         95th percentile:                        0.61
         sum:                                 9998.73

Threads fairness:
    events (avg/stddev):           1683.0000/0.00
    execution time (avg/stddev):   9.9987/0.00

