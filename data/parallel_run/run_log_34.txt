sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
1 files, 32GiB each
32GiB total file size
Block size 256KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     338.31
    fsyncs/s:                     3.44

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               84.58

General statistics:
    total time:                          10.1637s
    total number of events:              3473

Latency (ms):
         min:                                    0.13
         avg:                                    2.88
         max:                                  329.91
         95th percentile:                        0.31
         sum:                                 9997.24

Threads fairness:
    events (avg/stddev):           3473.0000/0.00
    execution time (avg/stddev):   9.9972/0.00

