sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
1 files, 16GiB each
16GiB total file size
Block size 512KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     163.33
    fsyncs/s:                     1.73

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               81.66

General statistics:
    total time:                          10.4046s
    total number of events:              1717

Latency (ms):
         min:                                    0.26
         avg:                                    6.06
         max:                                  616.23
         95th percentile:                        0.59
         sum:                                10402.67

Threads fairness:
    events (avg/stddev):           1717.0000/0.00
    execution time (avg/stddev):   10.4027/0.00

