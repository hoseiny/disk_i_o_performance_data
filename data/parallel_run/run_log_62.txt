sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 8GiB each
32GiB total file size
Block size 64KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     627.70
    fsyncs/s:                     25.21

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               39.23

General statistics:
    total time:                          10.0329s
    total number of events:              6549

Latency (ms):
         min:                                    0.03
         avg:                                    1.53
         max:                                  174.19
         95th percentile:                        0.15
         sum:                                10026.80

Threads fairness:
    events (avg/stddev):           6549.0000/0.00
    execution time (avg/stddev):   10.0268/0.00

