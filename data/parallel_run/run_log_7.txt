sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
1 files, 64GiB each
64GiB total file size
Block size 4KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     2006.40
    fsyncs/s:                     20.16

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               7.84

General statistics:
    total time:                          10.0155s
    total number of events:              20301

Latency (ms):
         min:                                    0.00
         avg:                                    0.49
         max:                                  265.53
         95th percentile:                        0.01
         sum:                                10004.08

Threads fairness:
    events (avg/stddev):           20301.0000/0.00
    execution time (avg/stddev):   10.0041/0.00

