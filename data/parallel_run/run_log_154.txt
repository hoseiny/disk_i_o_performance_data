sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
64 files, 1GiB each
64GiB total file size
Block size 128KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     396.30
    fsyncs/s:                     253.73

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               49.54

General statistics:
    total time:                          10.0896s
    total number of events:              6497

Latency (ms):
         min:                                    0.06
         avg:                                    1.55
         max:                                  499.42
         95th percentile:                        0.25
         sum:                                10070.72

Threads fairness:
    events (avg/stddev):           6497.0000/0.00
    execution time (avg/stddev):   10.0707/0.00

