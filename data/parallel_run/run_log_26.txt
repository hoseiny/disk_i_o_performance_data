sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
1 files, 16GiB each
16GiB total file size
Block size 128KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     480.97
    fsyncs/s:                     4.91

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               60.12

General statistics:
    total time:                          10.1840s
    total number of events:              4949

Latency (ms):
         min:                                    0.06
         avg:                                    2.06
         max:                                  234.45
         95th percentile:                        0.18
         sum:                                10179.34

Threads fairness:
    events (avg/stddev):           4949.0000/0.00
    execution time (avg/stddev):   10.1793/0.00

