sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 4GiB each
16GiB total file size
Block size 4KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     2137.10
    fsyncs/s:                     85.58

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               8.35

General statistics:
    total time:                          10.0099s
    total number of events:              22253

Latency (ms):
         min:                                    0.00
         avg:                                    0.45
         max:                                  234.84
         95th percentile:                        0.02
         sum:                                 9995.94

Threads fairness:
    events (avg/stddev):           22253.0000/0.00
    execution time (avg/stddev):   9.9959/0.00

