sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 512MiB each
2GiB total file size
Block size 4KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     1712.92
    fsyncs/s:                     68.62

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               6.69

General statistics:
    total time:                          10.0376s
    total number of events:              17885

Latency (ms):
         min:                                    0.00
         avg:                                    0.56
         max:                                  159.82
         95th percentile:                        0.02
         sum:                                10026.56

Threads fairness:
    events (avg/stddev):           17885.0000/0.00
    execution time (avg/stddev):   10.0266/0.00

