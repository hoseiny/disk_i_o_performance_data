sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
64 files, 64MiB each
4GiB total file size
Block size 4KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     1697.52
    fsyncs/s:                     1091.81

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               6.63

General statistics:
    total time:                          10.0130s
    total number of events:              27870

Latency (ms):
         min:                                    0.00
         avg:                                    0.36
         max:                                  169.72
         95th percentile:                        0.23
         sum:                                 9983.44

Threads fairness:
    events (avg/stddev):           27870.0000/0.00
    execution time (avg/stddev):   9.9834/0.00

