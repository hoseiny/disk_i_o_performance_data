sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 4GiB each
16GiB total file size
Block size 512KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     186.17
    fsyncs/s:                     7.54

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               93.08

General statistics:
    total time:                          10.2020s
    total number of events:              1973

Latency (ms):
         min:                                    0.18
         avg:                                    5.17
         max:                                  564.98
         95th percentile:                        0.59
         sum:                                10198.87

Threads fairness:
    events (avg/stddev):           1973.0000/0.00
    execution time (avg/stddev):   10.1989/0.00

