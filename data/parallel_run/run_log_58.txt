sysbench 1.0.17 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 1
Initializing random number generator from current time


Extra file open flags: (none)
4 files, 512MiB each
2GiB total file size
Block size 64KiB
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Initializing worker threads...

Threads started!


File operations:
    reads/s:                      0.00
    writes/s:                     706.44
    fsyncs/s:                     28.65

Throughput:
    read, MiB/s:                  0.00
    written, MiB/s:               44.15

General statistics:
    total time:                          10.0499s
    total number of events:              7386

Latency (ms):
         min:                                    0.03
         avg:                                    1.35
         max:                                  302.84
         95th percentile:                        0.14
         sum:                                 9993.95

Threads fairness:
    events (avg/stddev):           7386.0000/0.00
    execution time (avg/stddev):   9.9940/0.00

