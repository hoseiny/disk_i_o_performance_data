# README #


### What is this repository for? ###

* It holds data of expreimental results for Manuscript ID CPE-20-0411 entitled "Boosting Disk I/O Performance in Consolidated Virtualized Cloud Platforms." 
* Version 1.0
* Submitted Journal: Concurrency and Computation: Practice and Experience 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: reza.hoseiny@sydney.edu.au
