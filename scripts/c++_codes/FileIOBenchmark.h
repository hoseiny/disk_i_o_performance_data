/*
 * FileIOBenchmark.h
 *
 *  Created on: 3 Jan 2020
 *      Author: reza
 */
#ifndef FILEIOBENCHMARK_H_
#define FILEIOBENCHMARK_H_

#include <string>
#include<vector>
using namespace std;

enum file_test_mode_code {
	seqwr, //sequential write
	seqrewr, //sequential rewrite
	seqrd, //#                sequential read
	rndrd, //#                random read
	rndwr, //#                random write
	rndrw //#                combined random read/write
};

enum file_io_mode_code {
	//file operations mode {sync,async,mmap} [sync]
	sync,
	async,
	mmap
};

enum run_mode_code {
	save_result, dummy
};

class FileIOBenchmark {
private:
	int benchmarkId;
	FileIOBenchmark();
	string file_test_mode; //eg "rndrw"
	string file_io_mode; //eg "sync"
	int num_threads;
	int num_files;
	int file_blk_size;
	int file_total_size; // {2}

	int pause_between_stages { 2 };

	string _partial_command;

	string preplog_fname; //log file names for prep/run/clean up porcess to redirect the ouptut
	string runlog_fname;
	string cleanlog_fname;

	long double reads_per_s;
	long double writes_per_s;
	long double fsyncs_per_s;
	long double read_throughput_mb_per_s;
	long double write_throughput_mb_per_s;

	long double total_time;
	long double total_events;

	long double latency_min;
	long double latency_avg;
	long double latency_max;
	long double latency_p95;
	long double latency_sum;

	long double read_write_ratio;

	long double threads_fairness_events_avg_over_stddev;
	long double threads_fairness_execution_time_avg_over_stddev;

	string csv_filename;
	string delimerator { " , " };
	void _parse_result(string&);
	string _vector_to_string(vector<string> v);
public:
	int id();
	static int latest_benchmarkId;
	static bool csv_header_created; // a boolean flag to check if csv file exists and if so do nothing for creating header
	FileIOBenchmark(file_test_mode_code, file_io_mode_code, int _num_threads,
			int _num_files, int _file_blk_size, int _file_total_size,
			const char *_csv_filename);

	string prepare_command; // will be build based on _partial_command
	string run_command;
	string cleanup_command;
	void display();
	void run(run_mode_code mode);  //{"save_results", etc}
	string TestMode(file_test_mode_code);
	string IOMode(file_io_mode_code);
	void collect_result();
	void _make_csv_header();
	void append_csv();
	virtual ~FileIOBenchmark();
	FileIOBenchmark(const FileIOBenchmark &other);

};

void sleep(int n);

#endif /* FILEIOBENCHMARK_H_ */
