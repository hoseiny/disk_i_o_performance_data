/*
 * FileIOBenchmark.cpp
 *
 *  Created on: 3 Jan 2020
 *      Author: M.Reza HoseinyF.
 */

#include "FileIOBenchmark.h"
#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <fstream>
#include<vector>
#include <regex>
#include <fstream>
using namespace std;

int FileIOBenchmark::latest_benchmarkId = 0;
bool FileIOBenchmark::csv_header_created = false;

FileIOBenchmark::FileIOBenchmark(file_test_mode_code _file_test_mode,
		file_io_mode_code _file_io_mode, int _num_threads, int _num_files,
		int _file_blk_size, int _file_total_size, const char *_csv_filename) {

	this->benchmarkId = ++FileIOBenchmark::latest_benchmarkId;
	std::cout << "Constructor for Benchmark " << this->benchmarkId << endl;

	this->file_test_mode = std::string(TestMode(_file_test_mode));
	this->file_io_mode = std::string(IOMode(_file_io_mode));

	if (_num_threads > 0) {
		this->num_threads = _num_threads;
	} else {
		throw std::invalid_argument("Invalid syntax/argument for _num_threads");
	}
	if (_num_files > 0) {
		this->num_files = _num_files;
	} else {
		throw std::invalid_argument("Invalid syntax/argument for _num_files");
	}

	if (_file_blk_size > 0) {
		this->file_blk_size = _file_blk_size;
	} else {
		throw std::invalid_argument(
				"Invalid syntax/argument for _file_blk_size");
	}
	if (_file_total_size > 0) {
		this->file_total_size = _file_total_size;
	} else {
		throw std::invalid_argument(
				"Invalid syntax/argument for _file_total_size");
	}
	this->_partial_command = std::string("sysbench ") + " --threads="
			+ std::to_string(this->num_threads) + " --test=fileio"
			+ " --file-num=" + std::to_string(this->num_files)
			+ " --file-block-size=" + std::to_string(this->file_blk_size) + "K"
					" --file-total-size="
			+ std::to_string(this->file_total_size) + "G" + " --file-test-mode="
			+ (this->file_test_mode) + " --file-io-mode="
			+ (this->file_io_mode);

	this->preplog_fname = "./output/prepare_log_"
			+ std::to_string(this->benchmarkId) + ".txt";
	this->runlog_fname = "./output/run_log_" + std::to_string(this->benchmarkId)
			+ ".txt";
	this->cleanlog_fname = "./output/cleanup_log_"
			+ std::to_string(this->benchmarkId) + ".txt";
	this->prepare_command = this->_partial_command + " prepare" + " > "
			+ this->preplog_fname;
	this->run_command = this->_partial_command + " run" + " > "
			+ this->runlog_fname;
	this->cleanup_command = this->_partial_command + " cleanup" + " > "
			+ this->cleanlog_fname;
	string s(_csv_filename);
	this->csv_filename = s;
}

void FileIOBenchmark::run(run_mode_code mode) {
	switch (mode) {
	case save_result: {
		cout << "\n\033[1;34m running benchmark " << this->benchmarkId
				<< " and saving the result into log files...\033[0m" << endl;
		int ret1 = system(this->prepare_command.c_str());
		sleep(this->pause_between_stages);
		int ret2 = system(this->run_command.c_str());
		cout << "\n \033[4;35m running benchmark " << this->benchmarkId
				<< " Finished! now start cleanup...\033[0m" << endl;
		sleep(this->pause_between_stages);
		int ret3 = system(this->cleanup_command.c_str());
		cout << ret1 << " " << ret2 << " " << ret3 << " ";
	}
		break;
	case dummy:
		sleep(2);
		break;
	default:
		cerr << "[Unknown Run Mode Type]" << endl;
	}
}

void FileIOBenchmark::collect_result() {
	cout << "\033[4;34m\ncollecting result of benchmark " << this->benchmarkId
			<< " and saving the result into csv file...\033[0m" << endl;
	try {
		std::ifstream myfile(this->runlog_fname);
		if (myfile.is_open()) {
			std::string str;
			while (std::getline(myfile, str)) {
				this->_parse_result(str);
			}
		}
	} catch (exception & e) {
		std::cerr
				<< "Exception opening/reading/closing file\t Failed reading file/Unable to open file "
				<< this->runlog_fname << std::endl;
		throw std::runtime_error("Failed opening/reading the File");
	}
}

int FileIOBenchmark::id(){
	return this->benchmarkId;
}

void FileIOBenchmark::_parse_result(string &str) {
	vector<string> indicators { "reads/s", "writes/s", "fsyncs/s",
			"read, MiB/s", "written, MiB/s", "total time",
			"total number of events", "min:", "avg:", "max:",
			"95th percentile:", "sum:", "events (avg/stddev):",
			"execution time (avg/stddev):" };
	std::string indicator();
	int i = 0;
	for (auto indicator : indicators) {
		// "try to find " << indicator << "\n";
		std::size_t found_pos = str.find(indicator);
		if (found_pos != std::string::npos) // npos indicate no matches
				{
			// "first " indicator  " found at: " found_pos in: " str
			std::string substr = str.substr(found_pos + indicator.size() - 1,
					str.size()); //sub string from the postion that we foudn the inidcator towards the end of the line
			std::regex re("[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)"); // full float number match
			std::smatch match;
			if (std::regex_search(substr, match, re) && match.size() > 1) //"a string literal matched"
					{
				auto result = match.str(1);
				// std::cout << indicators[i] << indicator << " " << result << endl;
				std::size_t offset = 0;
				switch (i) {
				case 0: {
					this->reads_per_s = std::stold(result, &offset); //long double
				}
					break;
				case 1: {
					this->writes_per_s = std::stold(result, &offset);
				}
					break;
				case 2: {
					this->fsyncs_per_s = std::stold(result, &offset);
				}
					break;
				case 3: {
					this->read_throughput_mb_per_s = std::stold(result,
							&offset);
				}
					break;
				case 4: {
					this->write_throughput_mb_per_s = std::stold(result,
							&offset);
				}
					break;
				case 5: {
					this->total_time = std::stold(result, &offset);
				}
					break;
				case 6: {
					this->total_events = std::stold(result, &offset);
				}
					break;
				case 7: {
					this->latency_min = std::stold(result, &offset);
				}
					break;
				case 8: {
					this->latency_avg = std::stold(result, &offset);
				}
					break;
				case 9: {
					this->latency_max = std::stold(result, &offset);
				}
					break;
				case 10: {
					this->latency_p95 = std::stold(result, &offset);
				}
					break;
				case 11: {
					this->latency_sum = std::stold(result, &offset);
				}
					break;
				case 12: {
					this->threads_fairness_events_avg_over_stddev = std::stold(
							result, &offset);
				}
					break;
				case 13: {
					this->threads_fairness_execution_time_avg_over_stddev =
							std::stold(result, &offset);
				}
					break;
				}
			} else {
				auto result = std::string("Not Found");
			}

		}
		++i;
	}
}

FileIOBenchmark::~FileIOBenchmark() {
	std::cout << "FileIOBenchmark Destructor\n";
}

// char * testCPUcmd = "sysbench --test=cpu --cpu-max-prime=20000 run";

string FileIOBenchmark::TestMode(file_test_mode_code v) {
	switch (v) {
	case seqwr:
		return "seqwr";
	case seqrewr:
		return "seqrewr";
	case seqrd:
		return "seqrd";
	case rndrd:
		return "rndrd";
	case rndwr:
		return "rndwr";
	case rndrw:
		return "rndrw";
	default:
		return "[Unknown TestMode Type]";
	}
}

string FileIOBenchmark::IOMode(file_io_mode_code v) {
	switch (v) {
	case sync:
		return "sync";
	case async:
		return "async";
	case mmap:
		return "mmap";
	default:
		return "[Unknown IOMode Type]";
	}
}

void FileIOBenchmark::display() {
	std::cout << this->prepare_command << endl;
	std::cout << this->run_command << endl;
	std::cout << this->cleanup_command << endl;

	std::cout << this->csv_filename << endl;
	std::cout << "reporting performance metrics for benchmark "
			<< this->benchmarkId << endl;
	std::cout << "reads_per_s " << this->reads_per_s << endl;
	std::cout << "writes/s " << this->writes_per_s << endl;
	std::cout << "fsyncs/s " << this->fsyncs_per_s << endl;
	std::cout << "read, MiB/s " << this->read_throughput_mb_per_s << endl;
	std::cout << "write, MiB/s " << this->write_throughput_mb_per_s << endl;
	std::cout << "total time " << this->total_time << endl;
	std::cout << "total number of events " << this->total_events << endl;
	std::cout << "Latency min: " << this->latency_min << endl;
	std::cout << "Latency avg: " << this->latency_avg << endl;
	std::cout << "Latency max: " << this->latency_max << endl;
	std::cout << "Latency 95th percentile:: " << this->latency_p95 << endl;
	std::cout << "Latency sum: " << this->latency_sum << endl;
	std::cout << "threads fairness events avg: "
			<< this->threads_fairness_events_avg_over_stddev << endl;
	std::cout << "threads fairness execution time_avg: "
			<< this->threads_fairness_execution_time_avg_over_stddev << endl;
}

void FileIOBenchmark::_make_csv_header() {
	std::ofstream csv_ofs;
	if (FileIOBenchmark::csv_header_created) { //file exists do nothing
		return;
	}
	csv_ofs.open(this->csv_filename, std::ofstream::out);
	// Declaring Vector of String type
	vector<string> headers;
	headers.push_back("scenario Id");
	headers.push_back("file_test_mode");
	headers.push_back("file_io_mode");
	headers.push_back("num_threads");
	headers.push_back("num_files");
	headers.push_back("file_blk_size");
	headers.push_back("file_total_size");
	headers.push_back("reads_per_sec");
	headers.push_back("writes_per_sec");
	headers.push_back("fsyncs_per_sec");
	headers.push_back("read_throughput_MiB_per_sec");
	headers.push_back("write_throughput_MiB_per_sec");
	headers.push_back("total time");
	headers.push_back("total number of events");
	headers.push_back("Latency min");
	headers.push_back("Latency avg");
	headers.push_back("Latency max");
	headers.push_back("Latency p-95th");
	headers.push_back("Latency sum");
	headers.push_back("threads fairness events avg");
	headers.push_back("threads fairness execution time avg");
	headers.push_back("execution phase start time");
	headers.push_back("execution phase finish time");
	string csv_header = _vector_to_string(headers);
	csv_ofs << csv_header << endl;
	csv_ofs.close();
	FileIOBenchmark::csv_header_created = true;//udpate the flag for the next benchmark to not touch the header again
}

string FileIOBenchmark::_vector_to_string(vector<string> v) {
	string str { };
	for (auto it = v.begin(); it != v.end(); ++it) {
		str += *it;
		if (it != v.end() - 1)
			str += this->delimerator;
	}
	return str;
}
void FileIOBenchmark::append_csv() {
	this->_make_csv_header();
	std::ofstream csv_ofs;
	csv_ofs.open(this->csv_filename, std::ofstream::app);
	vector<string> row_v;
	row_v.push_back(std::to_string(this->benchmarkId));
	row_v.push_back(this->file_test_mode);
	row_v.push_back(this->file_io_mode);
	row_v.push_back(std::to_string(this->num_threads));
	row_v.push_back(std::to_string(this->num_files));
	row_v.push_back(std::to_string(this->file_blk_size));
	row_v.push_back(std::to_string(this->file_total_size));
	row_v.push_back(std::to_string(this->reads_per_s));
	row_v.push_back(std::to_string(this->writes_per_s));
	row_v.push_back(std::to_string(this->fsyncs_per_s));
	row_v.push_back(std::to_string(this->read_throughput_mb_per_s));
	row_v.push_back(std::to_string(this->write_throughput_mb_per_s));
	row_v.push_back(std::to_string(this->total_time));
	row_v.push_back(std::to_string(this->total_events));
	row_v.push_back(std::to_string(this->latency_min));
	row_v.push_back(std::to_string(this->latency_avg));
	row_v.push_back(std::to_string(this->latency_max));
	row_v.push_back(std::to_string(this->latency_p95));
	row_v.push_back(std::to_string(this->latency_sum));
	row_v.push_back(
			std::to_string(this->threads_fairness_events_avg_over_stddev));
	row_v.push_back(
			std::to_string(
					this->threads_fairness_execution_time_avg_over_stddev));
	string row = _vector_to_string(row_v);
	cout << row << endl;
	csv_ofs << row << endl;
	csv_ofs.close();
}

void sleep(int n) {
	std::this_thread::sleep_for(std::chrono::milliseconds(n * 1000));
}

FileIOBenchmark::FileIOBenchmark() {
	// TODO Auto-generated constructor stub
}

FileIOBenchmark::FileIOBenchmark(const FileIOBenchmark &other) {
	// TODO Auto-generated constructor stub
}
