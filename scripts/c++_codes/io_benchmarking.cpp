#include <iostream>
#include <fstream>
#include "FileIOBenchmark.h"
#include <cstring>
#include <cstdlib>
using namespace std;


int get_last_run_id(const char *filename_last_ran_id) {
	std::ifstream infile(filename_last_ran_id); //last_ran_id_file;
	int ret = 1;
	string line;
	if (infile.is_open()) {
		while (infile >> line) {
			ret = stoi(line);
		}
		infile.close();
	} else {
		ret = 0;
	}
	return ret;
}

int main(int argc, char **argv) {
	const char filename_last_ran_id[] = "./output/last_id.txt";
	// Create a vector containing all possible file_test_mode_code
	std::vector<file_test_mode_code> v_file_test_mode_codes = { seqwr, seqrewr,
			seqrd, rndrd, rndwr, rndrw };
	std::vector<file_io_mode_code> v_file_io_mode_codes = { sync, async, mmap };
	std::vector<int> v_num_threads = { 1, 2, 4, 8, 16, 32, 64 };
	std::vector<int> v_num_files = { 1, 4, 16, 64 };
	std::vector<int> v_file_blk_sizes = { 4, 16, 64, 128, 256, 512 };
	std::vector<int> v_file_total_sizes = { 1, 2, 4, 8, 16, 32, 64 };

	int last_ran_id = get_last_run_id(filename_last_ran_id);
	cout << last_ran_id << '\n';
//	exit(0);
	try { // Iterate over values of vector
		for (auto file_test_mode : v_file_test_mode_codes) {
			for (auto file_io_mode : v_file_io_mode_codes) {
				for (int num_threads : v_num_threads) {
					for (int num_files : v_num_files) {
						for (int file_blk_size : v_file_blk_sizes) {
							for (int file_total_size : v_file_total_sizes) {
								std::cout << file_test_mode << '\t'
										<< file_io_mode << '\t' << num_threads
										<< '\t' << num_files << '\t'
										<< file_blk_size << '\t'
										<< file_total_size << '\t' << endl;

								FileIOBenchmark *bptr = new FileIOBenchmark(
										file_test_mode, file_io_mode,
										num_threads, num_files, file_blk_size,
										file_total_size,
										"./output/results.csv");
								if (bptr->id() > last_ran_id) {
									cout << "running round " << bptr->id()
											<< endl;
									bptr->run(save_result);
									bptr->collect_result();
									bptr->display();
									bptr->append_csv();
									cout << "End of Exec. round wait 200 sec\n";
									sleep(200);
								} else {
									cout << "already performed round "
											<< bptr->id()
											<< " go for next round" << endl;
								}

							}
						}
					}
				}
			}
		}
	} catch (const std::invalid_argument &ia) {
		std::cerr << "Invalid argument: " << ia.what() << '\n';
	} catch (const std::runtime_error &re) {
		// specific handling for runtime_error
		std::cerr << "Runtime error: " << re.what() << std::endl;
	} catch (const std::exception &ex) {
		// specific handling for all exceptions extending std::exception, except std::runtime_error which is handled explicitly
		std::cerr << "Error occurred: " << ex.what() << std::endl;
	} catch (...) {
		// catch any other errors (that we have no information about)
		std::cerr << "Unknown failure occurred. Possible memory corruption"
				<< std::endl;
	}
	std::cout << "End of main\nFINISH\n";
	return 0;
}
