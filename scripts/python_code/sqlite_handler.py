import sqlite3
import os
from sqlite3 import Error
from globals import *s

class SQLiteHanlder:
    def __init__(self, _db_file):
        self.db_file = _db_file
        self.remove_existing_db()

    def remove_existing_db(self):
        try:
            os.remove(self.db_file)
            print("db_file EXISTS and removed!")
        except OSError:
            pass
        # if self.db_file EXISTS:
        #     self.db_file
        

    def create_connection(self):
        """ create a database connection to a SQLite database """
        self.conn = None
        try:
            self.conn = sqlite3.connect(self.db_file)
            print(sqlite3.version)
        except Error as e:
            print(e)

    def create_table(self, create_table_sql):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        :return:
        """
        try:
            c = self.conn.cursor()
            c.execute(create_table_sql)
        except Error as e:
            print(e)

    def createTables(self):
        self.sql_create_scenarios_table = """ CREATE TABLE IF NOT EXISTS scenarios (
                                        scenarioId integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                                        host_name text NOT NULL,                                                                              
                                        maxThreads  integer NOT NULL,
                                        interval integer NOT NULL,
                                        numFiles integer NOT NULL,
                                        file_block_size text NOT NULL,
                                        file_total_size text NOT NULL,
                                        file_test_mode text NOT NULL, 
                                        file_io_mode text NOT NULL, 
                                        file_rw_ratio NUMERIC NOT NULL, 
                                        start_time_stamp TIMESTAMP NOT NULL,
                                        end_time_stamp TIMESTAMP NOT NULL
                                    ); """
        #  test mode {seqwr, seqrewr, seqrd, rndrd, rndwr, rndrw}
        # file operations mode {sync,async,mmap} [sync]

        self.sql_create_perf_output_table = """CREATE TABLE IF NOT EXISTS performance_outputs (
                                    id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                                    scenarioId integer NOT NULL,
                                    start_time_stamp TIMESTAMP NOT NULL,
                                    end_time_stamp TIMESTAMP NOT NULL,
                                    FOREIGN KEY(scenarioId) REFERENCES scenarios(scenarioId)                                    
                                );"""

        self.sql_create_config_table = """CREATE TABLE IF NOT EXISTS configs (
                                    id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                                    key text NOT NULL,
                                    value text NOT NULL,
                                    comment text
                                );"""

        self.create_connection()        # create a database connection
        if self.conn is not None:
            # create tables
            self.create_table(self.sql_create_config_table)
            self.create_table(self.sql_create_scenarios_table)
            self.create_table(self.sql_create_perf_output_table)
        else:
            print("Error! cannot create the database connection.")

    def delete_config(self, _key):
        """
        Delete a task by task id
        :param conn:  Connection to the SQLite database
        :param id: id of the task
        :return:
        """
        sql = 'DELETE FROM configs WHERE key=?'
        cur = self.conn.cursor()
        cur.execute(sql, (_key,))
        self.conn.commit()

    def insert_or_update_config(self, config_tuple):
        """
        Create a new config vlaue into the configs table
        :param conn:
        :param project:
        :return: project id
        """
        if self.conn is None:
            self.create_connection()
        print('config_tuple\t', config_tuple)
        self.delete_config(config_tuple[0])
        sql = ''' INSERT INTO configs(key,value,comment)
                VALUES(?,?,?) '''
        cur = self.conn.cursor()
        cur.execute(sql, config_tuple)
        self.conn.commit()
        return cur.lastrowid

    def last_scenario_id(self):
        if self.conn is None:
            self.create_connection()
        """
        Query all rows in the configs table
        :param conn: the Connection object
        :return:
        """
        cursor = self.conn.cursor()
        cursor.execute('SELECT * FROM configs WHERE key=?',
                       ('last_scenario_id', ))
        print('cursor\t', cursor)
        rows = cursor.fetchall()
        print('rows\t', rows)
        if len(rows) is 0:
            cursor.close()
            return 0
        for row in rows:
            print(row)
            return int(row[2])

    def create_new_scenario(self, options):
        if self.conn is None:
            self.create_connection()
        print('options\t', options)  
        raw_data_tuple = []             
        raw_data_tuple.append(hostname())
        
        sql = ''' INSERT INTO scenarios(host_name,maxThreads,interval,numFiles,file_block_size, file_total_size, file_test_mode, file_io_mode, file_rw_ratio,start_time_stamp)
                VALUES(?,?,?,?,?,?,?,?,?) '''
        cur = self.conn.cursor()
        print(raw_data_tuple)
        cur.execute(sql, raw_data_tuple)
        self.conn.commit()
        return cur.lastrowid

    def closeDb(self):
        if self.conn:
            self.conn.close()
