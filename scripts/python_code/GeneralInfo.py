import platform, time
import psutil

# Architecture
print("Architecture: " + platform.architecture()[0])

# machine
print("Machine: " + platform.machine())

# node
print("Node: " + platform.node())

# system
print("System: " + platform.system())

# distribution
dist = platform.dist()
dist = " ".join(x for x in dist)
print("Distribution: " + dist)

# processor
print("Processors: ")
with open("/proc/cpuinfo", "r") as f:
    info = f.readlines()
    print(info)
# Processors:
#     0:  Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz
#     1:  Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz

cpuinfo = [x.strip().split(":")[1] for x in info if "model name" in x]
for index, item in enumerate(cpuinfo):
    print("    " + str(index) + ": " + item)


# Load using /proc/loadavg
# The first three fields in this file are load average figures giving the number of jobs in the run queue (state R) or waiting for disk I/O (state D) averaged over 1, 5, and 15 minutes. They are the same as the load average numbers given by uptime(1) and other programs.
# The first three columns measure CPU and IO utilization of the last one, five, and 15 minute periods. The fourth column shows the number of currently running processes and the total number of processes. The last column displays the last process ID used.
# The fourth field consists of two numbers separated by a slash (/). The first of these is the number of currently executing kernel scheduling entities (processes, threads); this will be less than or equal to the number of CPUs. The value after the slash is the number of kernel scheduling entities that currently exist on the system.
# The fifth field is the PID of the process that was most recently created on the system.

with open("/proc/loadavg", "r") as f:
    print("Average Load: " + f.read().strip())

print(psutil.cpu_count())
print(psutil.cpu_count(logical=False))
print(psutil.cpu_stats())
print(psutil.cpu_freq(percpu=True))
print("getloadavg\t", psutil.getloadavg())
print([x / psutil.cpu_count() * 100 for x in psutil.getloadavg()])
# gives a single float value
print("cpu_percent\t", psutil.cpu_percent())
print("cpu_times\t", psutil.cpu_times())
for x in range(2):
    print("Avg cpu %\tinterval {}\t{}".format(x, psutil.cpu_percent(interval=1)))
    print(
        "cpu % per cpu\tinterval {}\t{}".format(
            x, psutil.cpu_percent(interval=1, percpu=True)
        )
    )
    print("cpu_times\tinterval {}\t{}".format(x, psutil.cpu_times()))
    print("\n")
# gives an object with many fields
mem = psutil.virtual_memory()
print("virtual_memory\t", mem)
print("swap_memory\t", psutil.swap_memory())
# you can convert that object to a dictionary
print("virtual_memory _asdict\t", dict(psutil.virtual_memory()._asdict()))
[print(x) for x in psutil.disk_partitions()]
p = psutil.Process()
for x in range(3):
    print(p.memory_info())
    print(p.memory_full_info())
    print(psutil.disk_io_counters(perdisk=True))
    print(psutil.net_io_counters())
    print(psutil.net_connections())
    print(psutil.net_if_addrs())
    print(psutil.sensors_temperatures())
    time.sleep(1)
print(p.children(recursive=True))
print(p.open_files())
print(psutil.users())
for proc in psutil.process_iter():
    try:
        pinfo = proc.as_dict(attrs=["pid", "name", "username"])
    except psutil.NoSuchProcess:
        pass
    else:
        print(pinfo)

print("All mounted disk partitions\n")
disk_partitions = psutil.disk_partitions()
for partition in disk_partitions:
    for k, v in partition._asdict().items():
        print("{} \t {}".format(k, v))
    print("\n=====\n")

print("Disk usage statistics\t {}".format(psutil.disk_usage("/")))
print("\nsystem-wide disk I/O statistics\n")
# print("system-wide disk I/O statistics\t",psutil.disk_io_counters(perdisk=True))
disk_io_counters = psutil.disk_io_counters()
for k, v in disk_io_counters._asdict().items():
    print("{} \t {}".format(k, v))

