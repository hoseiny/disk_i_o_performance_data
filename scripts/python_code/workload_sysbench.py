from fabric.api import run, env, sudo, puts
from fabric.api import * 
import time
import envhosts

def CPU_workload_sysbench(max_prime_num, num_threads):
    cmd = "sysbench --test=cpu --cpu-max-prime=" + \
        str(max_prime_num) + " --num-threads=" + str(num_threads) + " run"
    print(cmd)
    sudo(cmd)

@parallel
def runs_sysbench_in_parallel(fsize=2, maxtime=30, outputfile='r.txt'):
    cmd2 = "sysbench --test=fileio --file-total-size=" + \
        str(fsize)+"G --file-test-mode=rndrw --max-time=" + \
        str(maxtime)+" --max-requests=0 run > " + outputfile
    # sudo(cmd2)
    print(cmd2)
    time.sleep(envhosts.refresh)

def Fileio_workload_sysbench_prepare(fsize=2, maxtime=30):
    cmd1 = "sysbench --test=fileio --file-total-size=" + str(fsize)+"G prepare"
    sudo(cmd1)
    time.sleep(envhosts.refresh)


def Fileio_workload_sysbench_seq_run(fsize=2, maxtime=30, outputfile='r.txt'):
    cmd2 = "sysbench --test=fileio --file-total-size=" + \
        str(fsize)+"G --file-test-mode=rndrw --max-time=" + \
        str(maxtime)+" --max-requests=0 run  > " + outputfile
    sudo(cmd2)
    time.sleep(envhosts.refresh)
