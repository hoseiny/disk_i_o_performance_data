from fabric.api import env, run, task
import datetime


# environment
env.use_ssh_config = True

# options
TIME = "600"                  # use 600    default = 10     seconds; --time is same as deprecated --max-time
THREADS = "4"                 # use 4      default = 1      min 2xnum of cpus; increase thread count till results topout
EVENTS = "0"                  # use 0      default = 0      limit total num of requests. 0 means no limit
FILE_NUM = "128"              # use 128    default = 128    number of files to create
PERCENTILE = "95"             # use 95     default = 95     95% is recommended
FILE_TOTAL_SIZE = "50G"       # use 50G    default = n/a    use at least a size of 2x amount of RAM
FILE_BLOCK_SIZE_RND = "16K"   # use 16K    default = 16384  use for random read/write
FILE_BLOCK_SIZE_SEQ = "128K"  # use 128K   default = 16384  use for sequential read/write
FILE_IO_MODE = "sync"         # use sync   default = sync   options = sync, async, mmap

# file-test-mode
SEQ_READ = "seqrd"            # sequential read
SEQ_WRITE = "seqwr"           # sequential write
RND_READ = "rndrd"            # random read
RND_WRITE = "rndwr"           # random write
RND_READ_WRITE = "rndrw"      # rw-ratio is 1.5 default

# sysbench output saved in this local vm folder
vm_local_folder = "/usr/share/sysbench_thesis/"


@task
def seq_read():
    sysbench_command(SEQ_READ, FILE_BLOCK_SIZE_SEQ)


@task
def seq_write():
    sysbench_command(SEQ_WRITE, FILE_BLOCK_SIZE_SEQ)


@task
def rnd_read():
    sysbench_command(RND_READ, FILE_BLOCK_SIZE_RND)


@task
def rnd_write():
    sysbench_command(RND_WRITE, FILE_BLOCK_SIZE_RND)


@task
def rnd_read_write():
    sysbench_command(RND_READ_WRITE, FILE_BLOCK_SIZE_RND)


def sysbench_command(file_test_mode, file_block_size):
    sysbench_cmd = str(
        "sysbench" +
        " --file-test-mode=" + file_test_mode +
        " --file-block-size=" + file_block_size +
        " --file-total-size=" + FILE_TOTAL_SIZE +
        " --file-io-mode=" + FILE_IO_MODE +
        " --file-num=" + FILE_NUM +
        " --threads=" + THREADS +
        " --time=" + TIME +
        " --events=" + EVENTS +
        " --percentile=" + PERCENTILE +
        " --histogram" +
        " fileio run"
    )
    sysbench_run(sysbench_cmd, file_test_mode)


def sysbench_run(sysbench_cmd, file_test_mode):

    host_name = run("hostname")

    # reverse date and normal time
    t = datetime.datetime.now().strftime("_%Y_%m_%d_%H_%M_%S")

    # file name: <linux-vmX>_<file-test-mode>_<reverse date and normal time>.txt
    append_to_file = " |& tee -a " + vm_local_folder + host_name + "_" + file_test_mode + t + ".txt"

    # additional info to append
    run("echo " + append_to_file)
    run("hostname" + append_to_file)
    run("echo " + append_to_file)
    run("echo File Test Mode: " + str(get_file_mode(file_test_mode)) + append_to_file)
    run("echo " + append_to_file)
    run("date" + append_to_file)
    run("echo " + append_to_file)

    # prepare files
    prepare_cmd = sysbench_prepare(append_to_file)

    # write command details into file
    run("echo " + append_to_file)
    run("echo Commands:" + append_to_file)
    run("echo " + prepare_cmd + append_to_file)
    run("echo " + sysbench_cmd + append_to_file)
    run("echo " + append_to_file)

    # run sysbench benchmark
    run(sysbench_cmd + append_to_file)

    print("Results in " + host_name + ": " + vm_local_folder + host_name + "_" + file_test_mode + t + ".txt")


@task
def sysbench_prepare(append_to_file):
    sysbench_cmd = "sysbench --file-total-size=" + FILE_TOTAL_SIZE + " --file-num=" + FILE_NUM + " fileio prepare"
    run(sysbench_cmd + append_to_file)  # if files already exist sysbench reuses them
    return sysbench_cmd


@task
def sysbench_cleanup():
    sysbench_cmd = "sysbench fileio cleanup"
    run(sysbench_cmd)


@task
def check_hosts():
    run("uptime")
    run("sysbench --version")


def get_file_mode(file_mode):

    file_test_mode = {
        'seqrd': 'SEQ_READ - Sequential Read',
        'seqwr': 'SEQ_WRITE - Sequential Write',
        'rndrd': 'RND_READ - Random Read',
        'rndwr': 'RND_WRITE - Random Write',
        'rndrw': 'RND_READ_WRITE - Combined Random Read and Write',
    }
    return file_test_mode[file_mode]
