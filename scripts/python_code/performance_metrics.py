import re

def find_performance_metric(metric_str, file_content):
    for line in file_content.splitlines():
        # print(line)
        search_str = r"\s*[\w\d.]*" + metric_str + r"\s*.*\s*"
        raw_s = r'{}'.format(search_str)
        # print(raw_s)
        # search = r"\s*reads/s:\s*.*\s*"
        matchObj = re.match(raw_s, line, re.M | re.I)
        if matchObj:
            str_part = matchObj.group()
        #     print(str_part)
            searchObj = re.findall(r"\d+[.]*\d*", str_part)
            if searchObj:
                metric_value = searchObj[0]
                if(metric_str == r"th percentile:"):
                    #     print(metric_str)
                    metric_value = searchObj[1]
                # print(metric_value)
            return float(metric_value)
        else:
            assert('Not found')


def find_reads_per_second(file_content):
    return find_performance_metric(r"reads/s:", file_content)


def find_writes_per_second(file_content):
    return find_performance_metric(r"writes/s:", file_content)


def find_readThroughput(file_content):
    return find_performance_metric(r"read, MiB/s:", file_content)


def find_writeThroughput(file_content):
    return find_performance_metric(r"written, MiB/s:", file_content)


def find_total_time(file_content):
    return find_performance_metric(r"total time:", file_content)


def find_total_event_number(file_content):
    return find_performance_metric(r"total number of events:", file_content)


def find_avg_latency(file_content):
    return find_performance_metric(r"avg:", file_content)


def find_95th_latency(file_content):
    return find_performance_metric(r"th percentile:", file_content)
